package utils

import krpc.client.Connection
import krpc.client.services.SpaceCenter
import utils.log.Log


class KspProgram private constructor(
        val connection: Connection,
        val spaceCenter: SpaceCenter,
        val vessel: SpaceCenter.Vessel
) {

    private val stageChecker = VesselStageChecker(this)

    companion object {

        private fun create(): KspProgram? {
            val connection = Utils.tryOrElse({
                Connection.newInstance("KSP program")
            }, {
                Log.e("Error while connecting to KSP", it)
                null
            }) ?: return null

            val spaceCenter = SpaceCenter.newInstance(connection)
            val vessel = spaceCenter.activeVessel

            if (vessel == null) {
                Log.w("There is no active vessel")
                return null
            }

            vessel.control.sas = true
            vessel.control.rcs = true

            return KspProgram(connection, spaceCenter, vessel)
        }

        fun run(action: KspProgram.() -> Unit) {
            val kspProgram = create() ?: return

            Utils.tryOrElse({
                action.invoke(kspProgram)
            }, {
                Log.e("Error while executing program", it)
            })

            kspProgram.connection.close()
        }

    }

    fun checkStageAndDecuple() = stageChecker.checkAndDecuple()

}
