package utils

import org.javatuples.Triplet


object Utils {

    fun <T> tryOrElse(action: () -> T, elseAction: (ex: Exception) -> T) =
            try {
                action.invoke()
            } catch (ex: Exception) {
                elseAction.invoke(ex)
            }

    fun <T> tryOrNull(action: () -> T) = tryOrElse(action, { null })

    fun cos(v1: Triplet<Double, Double, Double>, v2: Triplet<Double, Double, Double>): Double {
        val v1Length = length(v1)
        val v2Length = length(v2)
        if (v1Length <= 0 || v2Length <= 0) {
            return 0.0
        }
        val vdot = vdot(v1, v2)
        return vdot / (v1Length * v2Length)
    }

    fun length(v: Triplet<Double, Double, Double>) = Math.sqrt(
            (0 until v.size).sumByDouble { (v.getValue(it) as Double).let { it * it } }
    )

    fun vdot(v1: Triplet<Double, Double, Double>, v2: Triplet<Double, Double, Double>) =
            (0 until v1.size).sumByDouble { v1.getValue(it) as Double * v2.getValue(it) as Double }
}