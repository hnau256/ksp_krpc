package utils

import krpc.client.services.SpaceCenter


object KspUtils {

    private val PAUSE_MS = 10L

    fun sleep(condition: () -> Boolean) {
        while (condition.invoke()) pause()
    }

    fun sleepTime(program: KspProgram, time: Double) = with(program) {
        val end = spaceCenter.ut + time
        sleep { spaceCenter.ut < end }
    }

    fun pause() = Thread.sleep(PAUSE_MS)

    fun warpByRails(program: KspProgram, time: Double) = with(program) {
        val start = spaceCenter.ut + 1
        sleep { spaceCenter.ut < start }
        spaceCenter.warpTo(time - 1, 100000f, 4f)
        sleep { spaceCenter.ut < time }
    }

}