package utils.executors

import utils.KspProgram
import utils.KspUtils
import utils.log.Log


object ToOrbitExecutor {

    fun toOrbit(program: KspProgram, orbitHeight: Long, minRotatingApoapsis: Long, maxRotatingApoapsis: Long) = with(program) {
        vessel.control.activateNextStage()
        moveApoapsisToOrbitHeight(this, orbitHeight, minRotatingApoapsis, maxRotatingApoapsis)
        waitAtmosphereEscape(this, vessel.orbit.body.atmosphereDepth.toLong())
        OrbitLifter.circulizeUp(this)
    }

    private fun moveApoapsisToOrbitHeight(program: KspProgram, orbitHeight: Long, minRotatingApoapsis: Long, maxRotatingApoapsis: Long) = with(program) {
        vessel.control.sas = false
        vessel.autoPilot.engage()
        vessel.control.throttle = 1f

        var apoapsis: Double
        do {
            apoapsis = vessel.orbit.apoapsisAltitude
            val rotatingPosRaw = (apoapsis - minRotatingApoapsis) / (maxRotatingApoapsis - minRotatingApoapsis)
            val rotatingPos = if (rotatingPosRaw < 0) 0.0 else if (rotatingPosRaw > 1) 1.0 else rotatingPosRaw
            val angle = (90 - 90 * rotatingPos).toFloat()
            vessel.autoPilot.targetPitchAndHeading(angle, 90f)

            checkStageAndDecuple()

            KspUtils.pause()

        } while (apoapsis < orbitHeight)

        vessel.control.throttle = 0f
        vessel.autoPilot.disengage()
        vessel.control.sas = true
        KspUtils.pause()

        Log.d("Altitude reached orbit height ($orbitHeight)")
    }

    private fun waitAtmosphereEscape(program: KspProgram, atmosphereHeight: Long) = with(program) {
        val flight = vessel.flight(vessel.referenceFrame)

        spaceCenter.physicsWarpFactor = 4
        KspUtils.sleep { flight.meanAltitude < atmosphereHeight }
        spaceCenter.physicsWarpFactor = 1

        Log.d("Atmosphere escaped ($atmosphereHeight)")
    }

}