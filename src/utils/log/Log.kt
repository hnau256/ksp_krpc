package utils.log

import java.io.PrintWriter
import java.io.StringWriter


object Log {

    enum class Type(val color: AnsiColor) {
        DEBUG(AnsiColor.RESET),
        INFO(AnsiColor.GREEN),
        WARN(AnsiColor.YELLOW),
        ERROR(AnsiColor.RED)
    }

    fun d(text: String, ex: Exception? = null) = log(Type.DEBUG, text, ex)

    fun i(text: String, ex: Exception? = null) = log(Type.INFO, text, ex)

    fun w(text: String, ex: Exception? = null) = log(Type.WARN, text, ex)

    fun e(text: String, ex: Exception? = null) = log(Type.ERROR, text, ex)

    private fun log(type: Type, text: String, ex: Exception? = null) {
        val string = type.color.createString(text)
        System.out.println(string)
        ex?.let {
            val exceptionLinePrefix = "|   "
            val exceptionStr = getExceptionStackTrace(it)
            val exceptionOutStr = type.color.createString(
                    "$exceptionLinePrefix=== Exception ===\n" +
                            exceptionLinePrefix + exceptionStr.replace("\n", "\n" + exceptionLinePrefix) +
                            "================="
            )
            System.out.println(exceptionOutStr)
        }
    }

    private fun getExceptionStackTrace(ex: Exception): String {
        val sw = StringWriter()
        val pw = PrintWriter(sw)
        ex.printStackTrace(pw)
        return sw.toString()
    }


}